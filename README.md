# README

## About ##
a python script that parses GoAccess output (in json format) and summarizes data for LogWatch.

## Requirements ##

- install requirements: `pip install -r requirements.txt`


## Usage

- basic: `./golomo.py example/goaccess-report.json`
- the full logwatch implementation:
  - create the golomo.txt file via logrotate (in `/etc/logrotate.d/nginx`)
```
/var/log/nginx/*log {
    create 0644 nginx nginx
    daily
    rotate 14
    missingok
    notifempty
    compress
    compresscmd /usr/bin/xz
    compressext .xz
    uncompresscmd /usr/bin/unxz
    sharedscripts
    prerotate
      echo "- prerotate of file: ${f}, calling golomo..."
      /usr/bin/golomo ${f} > /var/log/nginx/golomo.txt
      echo "- golomo finished"
    endscript
    postrotate
      /bin/kill -USR1 `cat /run/nginx.pid 2>/dev/null` 2>/dev/null || true
    endscript
}
```
  - add the logwatch conf (in `/etc/logwatch/conf/logfiles/golomo.conf`)
```

```
  - add the logwatch service (in `/etc/logwatch/conf/services/golomo.conf`)
```

```
  - add the logwatch script (in `/etc/logwatch/scripts/services/golomo`)
```

```
  - make script executable: `chmod u+x /etc/logwatch/scripts/services/${service_name}`


## Example Output

Output is in markdown format so you can render it to html or other format easily.
```
./golomo.py example/goaccess-report.json
## GENERAL ##
- Start           : 11/Jun/2019
- End             : 11/Jun/2019
- Total           : 24,772
- Valid           : 24,772
- Failed          : 0
- Unique          : 1,147
- Bandwidth       : 330.703 MiB

## HTTP STATUS CODE ##
- 2xx Success     : 16974 (68.52%)
- 3xx Redirection : 6999 (28.25%)
- 4xx Client Error: 630 (2.54%)
- 5xx Server Error: 169 (0.68%)

## OS ##
- Unknown         : 16485 (66.55%)
- Windows         : 3248 (13.11%)
- Android         : 2279 (9.2%)
- Unix-like       : 1423 (5.74%)
- Macintosh       : 723 (2.92%)
- Darwin          : 273 (1.1%)
- iOS             : 266 (1.07%)
- Linux           : 64 (0.26%)
- Others          : 11 (0.04%)

## BROWSER ##
- Crawlers        : 18783 (75.82%)
- Chrome          : 1996 (8.06%)
- Safari          : 1003 (4.05%)
- Others          : 984 (3.97%)
- Unknown         : 916 (3.7%)
- MSIE            : 541 (2.18%)
- Firefox         : 536 (2.16%)
- Opera           : 13 (0.05%)

## TIME DISTRIBUTION ##
- 02              : 1227 (4.95%)
- 03              : 1759 (7.1%)
- 04              : 2006 (8.1%)
- 05              : 1268 (5.12%)
- 06              : 1612 (6.51%)
- 07              : 2346 (9.47%)
- 08              : 1716 (6.93%)
- 09              : 1904 (7.69%)
- 10              : 1792 (7.23%)
- 11              : 2030 (8.19%)
- 12              : 2584 (10.43%)
- 13              : 1756 (7.09%)
- 14              : 1541 (6.22%)
- 15              : 1231 (4.97%)
```


## Inspiration
- [GoAccess](https://goaccess.io/) - the 'static' html report from GoAccess is nice, a lot more data but the chrome around the data does not bode well in LogWatch
- [ngxtop](https://github.com/lebinh/ngxtop) - very near to what I needed ;-)
