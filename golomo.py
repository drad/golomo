#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  SPDX-License-Identifier: GPL-3.0-only
#  Copyright 2020 drad <drader@adercon.com>

import click
import json
import sys

standard_pad = 16
about = {
    "name": "golomo",
    "version": "1.1.0",
    "modified": "2020-02-16",
    "created": "2019-06-11",
}

sections = []

def format_number(n):
    return "{0:,g}".format(n)

@click.command()
@click.option("--time-distribution/--no-time-distribution", default=True, help="show (default) or do not show Time Distribution section")
@click.option("--browser/--no-browser", default=True, help="show (default) or do not show Browser section")
@click.option("--os/--no-os", default=True, help="show (default) or do not show OS section")
@click.option("--general/--no-general", default=True, help="show (default) or do not show General section")
@click.option("--status-codes/--no-status-codes", default=True, help="show (default) or do not show Status Codes section")
@click.option("--version", default=False, is_flag=True, help="show version and exit")
@click.argument('file', type=click.Path(exists=True))
def run(time_distribution, browser, os, general, status_codes, version, file):
    """
    This script parses a GoAccess json formatted output file and produces a concise, LogWatch friendly result.\n\n
    Arguments:\n
      FILE: the GoAccess json formatted output file to process.\n
    """
    if version:
        print("{0} - v.{1} ({2})".format(about["name"], about["version"], about["modified"]))
        sys.exit(0)

    with open(file, 'r') as json_file:
        json_data = json.load(json_file)

    section_general = []
    section_general.append( { "Start": json_data["general"]["start_date"] } )
    section_general.append( { "End": json_data["general"]["end_date"] } )
    section_general.append( { "Total": format_number(json_data["general"]["total_requests"]) } )
    #section_general.append( { "Valid": format_number(json_data["general"]["valid_requests"]) } )
    section_general.append( { "Failed": format_number(json_data["general"]["failed_requests"]) } )
    section_general.append( { "Unique": format_number(json_data["general"]["unique_visitors"]) } )
    #section_general.append( { "Bandwidth": format_number(json_data["general"]["bandwidth"] / 1024 / 1024) + " MiB" } )

    section_oses = []
    for item in json_data["os"]["data"]:
        section_oses.append( { "label": item["data"], "count": int(item["hits"]["count"]), "percent": "{0}".format(item["hits"]["percent"]) } )

    section_browsers = []
    for item in json_data["browsers"]["data"]:
        section_browsers.append( { "label": item["data"], "count": int(item["hits"]["count"]), "percent": "{0}".format(item["hits"]["percent"]) } )

    section_visit_times = []
    for item in json_data["visit_time"]["data"]:
        section_visit_times.append( { "label": item["data"], "count": int(item["hits"]["count"]), "percent": "{0}".format(item["hits"]["percent"]) } )

    section_status_codes = []
    for item in json_data["status_codes"]["data"]:
        section_status_codes.append( { "label": item["data"], "count": int(item["hits"]["count"]), "percent": "{0}".format(item["hits"]["percent"]) } )


    if general:
        print("## GENERAL ##")
        for item in section_general:
            print("- {0}: {1}".format( [x.ljust(standard_pad) for x in item.keys()][0], [y for y in item.values()][0] ))
    if status_codes:
        print("")
        print("## HTTP STATUS CODE ##")
        for item in sorted(section_status_codes, key= lambda i: i['label'], reverse=False):
            print("- {0}: {1} ({2}%)".format(item["label"].ljust(standard_pad), item["count"], item["percent"]))
    if os:
        print("")
        print("## OS ##")
        for item in sorted(section_oses, key= lambda i: i['count'], reverse=True):
            print("- {0}: {1} ({2}%)".format(item["label"].ljust(standard_pad), item["count"], item["percent"]))
    if browser:
        print("")
        print("## BROWSER ##")
        for item in sorted(section_browsers, key= lambda i: i['count'], reverse=True):
            print("- {0}: {1} ({2}%)".format(item["label"].ljust(standard_pad), item["count"], item["percent"]))
    if time_distribution:
        print("")
        print("## TIME DISTRIBUTION ##")
        for item in sorted(section_visit_times, key= lambda i: i['label'], reverse=False):
            print("- {0}: {1} ({2}%)".format(item["label"].ljust(standard_pad), item["count"], item["percent"]))

if __name__ == '__main__':
    run()
